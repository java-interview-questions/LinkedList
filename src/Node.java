/**
 * Created by faris on 8/7/2017.
 */
public class Node
{
    private int int_data;
    private double double_data;
    private String str_data;
    private char char_data;
    private Node next;

    public Node(int d)
    {
        this.int_data = d;
        this.next = next;
    }

    public Node(String d)
    {
        this.str_data = d;
    }

    public Node(double d)
    {
        this.double_data = d;
    }

    public Node(char d)
    {
        this.char_data = d;
    }

    public Node(int d, Node next)
    {
        this.int_data = d;
        this.next = next;
    }

    public Node(String d, Node next)
    {
        this.str_data = d;
        this.next = next;
    }

    public Node(double d, Node next)
    {
        this.double_data = d;
        this.next = next;
    }

    public Node(char d, Node next)
    {
        this.char_data = d;
        this.next = next;
    }

    public boolean haveNextNode()
    {
        if(next == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public String getElement()
    {
        if(this.str_data != null)
        {
            return this.str_data;
        }
        else if(this.double_data != 0.0)
        {
            return Double.toString(this.double_data);
        }
        else if(this.int_data != 0)
        {
            return Integer.toString(this.int_data);
        }
        else if(this.char_data != ' ')
        {
            return Character.toString(this.char_data);
        }

        return null;
    }

    public Node getNextNode()
    {
        return this.next;
    }
}
