/**
 * Created by faris on 8/7/2017.
 */
public class LinkedListImpl
{
    public static void main(String[] args)
    {
        //  Create a list of Linked List containing three digits
        Node first = new Node(1);
        Node second = new Node('2', first);
        Node third = new Node('3', second);

        displayElements(third);

        //  Search within a Linked List

        //  Remove Elements from a Linked List by Index

        //  Add Elements at a Specific Index in a Linked List

        //  Create a Doubly Linked List

        //  Reverse a Doubly Linked List
    }

    //  Display the Elements from a Linked List
    public static void displayElements(Node node)
    {
        if(node.haveNextNode())
        {
            System.out.print( node.getElement() + " . ");
            displayElements(node.getNextNode());
        }
        else if(!node.haveNextNode())
        {
            System.out.print( node.getElement());
        }
    }

    //  Remove Elements from a Linked List
    public static void removeElement(int node)
    {

    }

}
